import React from 'react';
import { SignUp } from './pages/sign-up';
import { SignIn } from './pages/sign-in';
import { HomeDefault } from './pages/home-default';
import { AddEvent } from './pages/add-event'
import { Verify } from './pages/verify';
import { ListEvents } from './pages';
import { EventSharing } from './pages/event-sharing'
import { Navigation } from './pages/navigation'
import Profile from './pages/profile'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { AuthUserProvider } from './providers/auth-user-provider';
import './style/grid.scss'
import './style/grid-align.scss'
import './style/main.scss';
import { VoteHistory } from './pages/vote-history';

const App = () => {
    return (
        <AuthUserProvider>
            <Navigation>
                <Router>
                    <Switch>
                        <Route path="/listevents">
                            <ListEvents />
                        </Route>
                        <Route path="/login">
                            <SignIn />
                        </Route>
                        <Route path="/register">
                            <SignUp />
                        </Route>
                        <Route path="/addEvent">
                            <AddEvent />
                        </Route>
                        <Route path="/verify">
                            <Verify />
                        </Route>
                        <Route path="/event-id">
                            <EventSharing />
                        </Route>
                        <Route path="/profile">
                            <Profile />
                        </Route>
                        <Route path="/vote-history">
                            <VoteHistory />
                        </Route>
                        <Route path="/">
                            <HomeDefault />
                        </Route>
                    </Switch>
                </Router>
            </Navigation>
        </AuthUserProvider>
    )
}

export default App
