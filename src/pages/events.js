import React from 'react'
import { useCol } from '../Hooks/firebase'
// import { Upvote } from '../components/upvote'
import { EventRender } from '../components'

export const ListEvents = () => {
    const { data } = useCol('Events');
    // const date = (new Date())

    return (
        <div className='flex-col'>
            {
                data && data.map((e, i) => <EventRender key={e.id} {...e}/>)
            }
        </div>
    )
}