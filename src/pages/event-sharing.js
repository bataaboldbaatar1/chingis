import React, { useState, useEffect } from 'react'
import { useDoc, useFirebase } from '../Hooks/firebase';
import {FacebookSharing} from '../components/facebook';
import { useStorage } from '../Hooks/use-storage'

export const EventSharing = () => {
    let url = new URLSearchParams(window.location.search).get('id')
    const imgUrl  = useStorage(`EventImages/${url}/MainImage.jpg`)
    const { data } = useDoc(`Events/${url}`);
    const { storage } = useFirebase();
    // const [imgUrl, setImgUrl] = useState('');
    const [sideImgUrl, setSideImgUrl] = useState([]);


    useEffect(() => {
        if (storage) {
            var storageRef = storage.ref();
            // var starsRef = storageRef.child(`EventImages/${url}/MainImage.jpg`);
            var sideImageRef = storageRef.child(`EventImages/${url}`);

            sideImageRef.listAll().then(async (res) => {
                setSideImgUrl(
                    await Promise.all(
                        res.items
                            .filter((itemRef) => itemRef.location.path_ !== `EventImages/${url}/MainImage.jpg`)
                            .map(async (itemRef) => await storageRef.child(itemRef.location.path_).getDownloadURL())
                    )
                )
            })

            // starsRef.getDownloadURL().then(function (url) {
            //     setImgUrl(url);
            // });
        }
    }, [storage, url])

    return (
        <div>
            <div style={{backgroundColor: 'gray', backgroundSize: 'cover', backgroundRepeat: 'no-repeat', backgroundImage: `url("${imgUrl}")`}} className='w-100 h-100' ></div>

            <h1>{data && data.name}</h1>
            <h1>{data && data.desc}</h1>

            <div className='flex'>
                {
                    sideImgUrl && sideImgUrl.map((e, i) => {
                        return (
                            <div style={{backgroundColor: 'gray', backgroundSize: 'cover', backgroundImage: `url("${e}")`}} className='simg w-100 h-100' key={i} ></div>
                        )
                    })
                }
            </div>

            <h3>Categroies</h3>
            <ul>
                {
                    data && data.categories.map((e, i) => {
                        return (
                            <li key={i}>{e}</li>
                        )
                    })
                }
            </ul>

            <div>Voted: {data && data.vote}</div>
            <FacebookSharing></FacebookSharing>

        </div>
    )
}