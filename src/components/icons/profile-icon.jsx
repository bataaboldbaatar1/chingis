import React from 'react'

export const ProfileIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M16.625 19V17C16.625 15.9391 16.2036 14.9217 15.4534 14.1716C14.7033 13.4214 13.6859 13 12.625 13H4.625C3.56413 13 2.54672 13.4214 1.79657 14.1716C1.04643 14.9217 0.625 15.9391 0.625 17V19M12.625 5C12.625 7.20914 10.8341 9 8.625 9C6.41586 9 4.625 7.20914 4.625 5C4.625 2.79086 6.41586 1 8.625 1C10.8341 1 12.625 2.79086 12.625 5Z" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}